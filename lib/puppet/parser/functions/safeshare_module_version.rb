module Puppet::Parser::Functions
  # Extract the first two components from the Semver from the "version"
  # field of the metadata.json file
  newfunction(:safeshare_module_version, :type => :rvalue) do |args|
    JSON.parse(
      File.read(
        Pathname.new(__FILE__).dirname.
          join('../../../../metadata.json')))['version'].
            split('.')[0..1].join('.')
  end
end
