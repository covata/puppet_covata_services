class safeshare::webapp::install inherits safeshare::webapp
{
  include safeshare::repo

  package { 'safe-share-for-web':
    ensure  => $safeshare::webapp::version,
    require => Class['safeshare::repo'],
  }

  if $safeshare::webapp::manage_nginx {
    class { '::nginx':
      worker_connections   => 20000,
      worker_processes     => '2',
      worker_rlimit_nofile => 20000,
      confd_purge          => true,
      server_purge         => true,
    }
  }
}
