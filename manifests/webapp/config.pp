class safeshare::webapp::config inherits safeshare::webapp
{
  if $safeshare::webapp::manage_nginx {

    if $safeshare::webapp::ssl {
      file { $safeshare::webapp::ssl_cert_path:
        ensure => 'directory',
        mode   => '0750',
        group  => 'root',
        owner  => 'root',
      }

      file { "${safeshare::webapp::ssl_cert_path}/${safeshare::webapp::ssl_cert_name}":
        ensure => file,
        source => "${safeshare::webapp::cert_source_path}/${safeshare::webapp::ssl_cert_name}",
        mode   => '0640',
        owner  => 'root',
        group  => 'root',
        notify => Service['nginx'],
      }

      file { "${safeshare::webapp::ssl_cert_path}/${safeshare::webapp::ssl_key_name}":
        ensure => file,
        source => "${safeshare::webapp::cert_source_path}/${safeshare::webapp::ssl_key_name}",
        mode   => '0640',
        owner  => 'root',
        group  => 'root',
        notify => Service['nginx'],
      }

      $ssl_cert = "${safeshare::webapp::ssl_cert_path}/${safeshare::webapp::ssl_cert_name}"
      $ssl_key = "${safeshare::webapp::ssl_cert_path}/${safeshare::webapp::ssl_key_name}"
      $port = $safeshare::webapp::https_port
      $ssl_port = $safeshare::webapp::https_port

    } else {
      $ssl_cert = undef
      $ssl_key = undef
      $port = $safeshare::webapp::nginx_port
      $ssl_port = '443'
    }

    if $safeshare::webapp::include_robots_header {
        $robots_header = 'add_header "X-Robots-Tag" "noindex, nofollow, noarchive, nosnippet";'
    } else {
        $robots_header = ''
    }


    # Setting all CORS headers
    # note: Access-Control-Allow-Origin is necessary for GET and OPTIONS;
    # due to https://www.nginx.com/resources/wiki/start/topics/depth/ifisevil/
    # we do have to replicate it
    nginx::resource::server { 'webapphost':
      www_root            => '/usr/share/safeshareweb/',
      index_files         => ['index.html'],
      ssl                 => $safeshare::webapp::ssl,
      ssl_cert            => $ssl_cert,
      ssl_key             => $ssl_key,
      ssl_port            => $ssl_port,
      listen_port         => $port,
      location_raw_append => [
        'add_header "Access-Control-Allow-Origin"  "*";',
        'add_header "X-Frame-Options" "DENY";',
        $robots_header,
        'if ($request_method = "OPTIONS") {
          add_header "Access-Control-Allow-Origin"  "*";
          add_header "Access-Control-Allow-Methods" "GET, OPTIONS";
          add_header "Access-Control-Allow-Headers" "Origin, Content-Type, Authorization";
          add_header "Content-Type" "text/plain charset=UTF-8";
          add_header "Content-Length" 0;
          return 204;
        }',
        'gzip on;
        gzip_min_length 1000;
        gzip_disable "msie6";
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;',
      ]
    }

    nginx::resource::location { '~* /(index\.html|themes\.json)$':
      server      => 'webapphost',
      # priority is used to keep this 'location' ahead of the next one,
      # otherwise it will be ignored by nginx
      priority    => 450,
      ssl         => $safeshare::webapp::ssl,
      ssl_only    => $safeshare::webapp::ssl,
      index_files => ['index.html'],
      www_root    => '/usr/share/safeshareweb/',
      raw_append  => [
        'add_header Expires "Thu, 01 Jan 1970 00:00:00 GMT";',
        'add_header Cache-Control "no-cache, no-store, must-revalidate";',
        'add_header Access-Control-Allow-Origin "*";',
        'add_header "X-Frame-Options" "DENY";',
        $robots_header,
      ],
    }

    nginx::resource::location { '~* \.(?:ico|css|js|gif|jpe?g|png|html)$':
      server      => 'webapphost',
      priority    => 460,
      ssl         => $safeshare::webapp::ssl,
      ssl_only    => $safeshare::webapp::ssl,
      index_files => ['index.html'],
      www_root    => '/usr/share/safeshareweb/',
      raw_append  => [
        'add_header "Access-Control-Allow-Origin"  "*";',
        'add_header "Expires" "365d";',
        'add_header "Pragma" "Public";',
        'add_header "Cache-Control" "public";',
        'add_header "X-Frame-Options" "DENY";',
        $robots_header
      ],
    }
  }

  file { '/usr/share/safeshareweb/config.json':
    ensure  => file,
    content => template('safeshare/usr/share/safeshareweb/config.json.erb'),
    mode    => '0644',
  }
}
