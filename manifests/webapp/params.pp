class safeshare::webapp::params
{
  $version                = 'latest'
  $manage_nginx           = true
  $nginx_port             = '80'
  $ssl                    = false
  $https_port             = 443
  $ssl_cert_path          = '/certs'
  $ssl_cert_name          = 'webapp.crt'
  $ssl_key_name           = 'webapp.key'
  $cert_source_path       = undef
  $access_service_url     = undef
  $custom_app_source_path = undef
  $themes_source_path     = undef
  $include_robots_header  = false

  if ! member(['RedHat', 'Debian'], $::osfamily) {
    fail("unsupported osfamily \"${::osfamily}\"")
  }
}
