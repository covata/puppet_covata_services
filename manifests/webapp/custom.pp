class safeshare::webapp::custom inherits safeshare::webapp {

  File {
    ensure  => 'directory',
    recurse => 'remote',
    group   => 'root',
    owner   => 'root',
    mode    => '0644',
    require => Class['safeshare::webapp::install'],
  }

  if $safeshare::webapp::custom_app_source_path {
    file { '/usr/share/safeshareweb':
      source  => $safeshare::webapp::custom_app_source_path,
    }
  }

  if $safeshare::webapp::themes_source_path {
    file { '/usr/share/safeshareweb/themes':
      source  => $safeshare::webapp::themes_source_path,
      purge   => true,
      recurse => true,
    }
  }
}
