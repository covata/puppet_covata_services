class safeshare::mvc_web::service {
  service {'mvc-web':
    ensure  => running,
    require => [
      Class['safeshare::mvc_web::package'],
      Class['safeshare::mvc_web::config']
    ],
  }
}
