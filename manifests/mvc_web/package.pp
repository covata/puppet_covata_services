class safeshare::mvc_web::package (
  $version
) {
  case $::osfamily {
    'RedHat': {
      $java_service_wrapper_package = 'tanukiwrapper'

      package {'mvc-web':
        ensure  => $version,
        require => [Package[$java_service_wrapper_package], Class['safeshare::repo']],
        notify  => [Service['mvc-web']],
      }
    }
    'Debian': {
      package {'mvc-web':
        ensure  => $version,
        require => [Class['safeshare::repo']],
        notify  => [Service['mvc-web']],
      }
    }
    default: { fail("Unsupported family ${::osfamily}") }
  }
}
