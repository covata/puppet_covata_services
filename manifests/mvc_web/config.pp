class safeshare::mvc_web::config (
  $access_service_url,
  $log_max_file_size,
  $log_days_max_history,
  $java_xmx = '2G',
  $min_firefox_version = undef,
  $company_name = undef,
  $tlsenabled = true,
  $help_url = undef,
  $http_port = 8082,
) {
  File {
    group   => 'mvc-web',
    owner   => 'mvc-web',
    mode    => 600,
    require => Class['safeshare::mvc_web::package'],
  }

  file {'/etc/mvc-web/loggerConfig.xml':
    ensure  => 'file',
    content => template('safeshare/mvc_web/loggerConfig.xml.erb'),
    notify  => Service['mvc-web'],
  }

  $wrapper_os_family = $::osfamily ? {
    'RedHat' => 'redhat',
    'Debian' => 'debian',
    default  => fail("unsupported osfamily ${::osfamily}")
  }

  file {'/etc/mvc-web/wrapper.conf':
    ensure  => 'file',
    content => template("safeshare/mvc_web/wrapper.${wrapper_os_family}.conf.erb"),
    notify  => Service['mvc-web'],
  }
}
