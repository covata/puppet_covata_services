# Class: safeshare::globals
#
# Set global configuration used by multiple SafeShare classes, including apt/yum repositories.
#
# If none of the repository settings are defined, no repository will be installed.
# But you'll still need to provide the packages in some other way.
#
# Paramaters:
#
#  [*repo_token*]
#    Gemfury.com access token. It's used to setup the software repositories.
#    Mandatory if installing Safe Share from Covata's gemfury repository.
#
#  [*alternative_apt_repository*]
#    APT repository to use instead of Covata's gemfury repository.
#    Leave it undefined if using Covata's gemfury repository.
#
#  [*alternative_apt_key*]
#    APT key ID to use with *alternative_apt_repository*.
#    Leave it undefined if using Covata's gemfury repository.
#
#  [*alternative_apt_key_source*]
#    APT key source to use with *alternative_apt_repository*.
#    Leave it undefined if using Covata's gemfury repository.
#
#  [*alternative_yum_repository*]
#    YUM repository to use instead of Covata's gemfury repository.
#    Leave it undefined if using Covata's gemfury repository.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class safeshare::globals (
  $repo_token = undef,
  $alternative_apt_repository = undef,
  $alternative_apt_key = undef,
  $alternative_apt_key_source = undef,
  $alternative_yum_repository = undef,
) {

  class { 'safeshare::repo':
    token                      => $repo_token,
    alternative_apt_repository => $alternative_apt_repository,
    alternative_apt_key        => $alternative_apt_key,
    alternative_apt_key_source => $alternative_apt_key_source,
    alternative_yum_repository => $alternative_yum_repository,
  }

  contain safeshare::repo
}
