# Class: safeshare::access_service_migration
#
#   Setup the tool which is used to maintain the Access Service' database
#   schema.
#   The tool has access to more database privileges which are not required by
#   the Access Service itself and therefore it is beneficial for the tool to
#   be installed on a separate server from the Access Service.
#
# Parameters:
#
#  [*version*]
#    Optional. Default value is `latest`.
#    The SafeShare Access Service Migration version to install.
#    Should be version 3.0 and above. Should include the full
#    version number (e.g. '3.0-965-1'). Can also be any possible
#    `Package` resource `ensure` value such as `latest` or `present`
#
#  [*webapp_service_url*]
#    URL of the Web app
#
#  [*access_service_url*]
#    URL of the Access Service.
#
#  [*database_password*]
#    The password of the database user used by the migration tool.
#    The database username is `so3`, it can't be changed via this module.
#
#  [*database_hostname*]
#    Optional. Default value is `localhost`.
#    The hostname of the database used by the migration tool if
#    required to connect to an external database instance.
#
#  [*database_name*]
#    Optional. Default value is `so`.
#    The name of the database used by the migration tool if
#    required to connect to an external database instance.
#
#  [*database_port*]
#    Optional. Default value is `5432`.
#    The port of the database used by the migration tool if
#    required to connect to an external database instance.
#
#  [*log_max_file_size*]
#    (Optional) Maximum log file size in bytes at which the file will be rotated.
#    Can be followed by optional suffixes 'KB', 'MB', 'GB'. Default is `30MB`.
#
#  [*log_days_max_history*]
#    (Optional) Maximum number of days to keep previous log files.
#
#  [*log_appenders*]
#    (Optional) A list of log appenders Access Service Migration will use to write logs to
#    Default: ['console', 'stdout']
#    Valid options are listed in Class: safeshare::params
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class safeshare::access_service_migration(
  $version               = $safeshare::access_service_migration::params::version,
  $access_service_url    = $safeshare::access_service_migration::params::access_service_url,
  $webapp_service_url    = $safeshare::access_service_migration::params::webapp_service_url,
  $database_password     = $safeshare::access_service_migration::params::database_password,
  $database_hostname     = $safeshare::access_service_migration::params::database_hostname,
  $database_name         = $safeshare::access_service_migration::params::database_name,
  $database_port         = $safeshare::access_service_migration::params::database_port,
  $log_max_file_size     = $safeshare::access_service_migration::params::log_max_file_size,
  $log_days_max_history  = $safeshare::access_service_migration::params::log_days_max_history,
  $log_appenders         = $safeshare::access_service_migration::params::log_appenders,
) inherits safeshare::access_service_migration::params {
  include safeshare::params

  if $webapp_service_url == undef {
    fail('webapp_service_url needs to be defined')
  }

  if $database_password == undef {
    fail('database_password needs to be defined')
  }

  if $access_service_url == undef {
    fail('access_service_url needs to be defined')
  }

  validate_re($log_max_file_size, '^[0-9]*([KMG]B)?$',
    'log_max_file_size must be numeric with optional "KB" or "MB" or "GB" suffix')

  validate_numeric($log_days_max_history)

  validate_array($log_appenders)
  if ! member($safeshare::params::valid_appenders, $log_appenders) {
    $invalid_appenders = join($log_appenders, ',')
    $allowed_appenders = join($safeshare::params::valid_appenders, ',')
    fail("log_appenders contains invalid values \"${invalid_appenders}\". Valid values are \"${allowed_appenders}\".")
  }

  anchor { 'safeshare::access_service_migration::begin': }
  -> class { 'safeshare::access_service_migration::install':}
  -> class { 'safeshare::access_service_migration::config': }
  -> class { 'safeshare::access_service_migration::migration': }
  -> anchor { 'safeshare::access_service_migration::end': }
}
