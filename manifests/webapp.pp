# Class: safeshare::webapp
#   Available from Covata version 3.1.
#
#   This manifest sets up the Safe Share webapp resources service.
#   This service contains file resources for the Safe Share webapp.
#   By default, this module will install these file resources and then
#   optionally install and set up NGINX to serve them.
#
#   Refer to `$manage_nginx` (below) for more information about disabling
#   the installation of NGINX (should you wish to support your own server
#   for these file resources).
#
# Parameters:
#
#  [*version*]
#    Defines the version of the Safe Share webapp resources service to install.
#    This parameter is optional, although its value must be a Covata 3.1 version or later.
#    If specifying a version number, it must be specified in full (e.g. '3.1-120-1').
#    This parameter's value can also be any valid `Package` resource `ensure` attribute
#    value such as `latest` or `present`.
#    If this parameter is not specified, its default value of `latest` is assumed.
#
#  [*access_service_url*]
#    URL of the Access Service, should have the same value as
#    `$safeshare::access_service::access_service_url`
#
#  [*manage_nginx*]
#    Determines if this manifest installs and configures NGINX to serve the
#    Safe Share webapp resources service (i.e. `true`) or not (i.e. `false`).
#    This parameter is optional, although if you specify `false`, you need to
#    install/configure an NGINX/Apache HTTP Server to serve the Safe Share webapp
#    resources service from `/usr/share/safeshareweb/`.
#    You also need to enable CORS on this NGINX/Apache HTTP Server to allow the Access
#    Service nodes to access the resources provided by the Safe Share webapp resources
#    service. Note that all Safe Share webapp resources are public.
#    For more information about CORS, see: http://enable-cors.org/ and
#    https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
#    If this parameter is not specified, its default value of `true` is assumed.
#
#  [*nginx_port*]
#    Defines the port on which the Safe Share webapp resources service answers when
#    `manage_nginx` is `true`.
#    This parameter is optional, although if it is not specified, its default value is
#    `80`.
#
#  [*custom_app_source_path*]
#    Optional source directory containing customized application static assets if html,
#    css etc needs to be overridden.
#
#  [*themes_source_path*]
#    Optional source directory containing customized application favicon,
#    logo images and application colours.
#

class safeshare::webapp(
  $version                = $safeshare::webapp::params::version,
  $access_service_url     = $safeshare::webapp::params::access_service_url,
  $manage_nginx           = $safeshare::webapp::params::manage_nginx,
  $nginx_port             = $safeshare::webapp::params::nginx_port,
  $custom_app_source_path = $safeshare::webapp::params::custom_app_source_path,
  $themes_source_path     = $safeshare::webapp::params::themes_source_path,
  $ssl                    = $safeshare::webapp::params::ssl,
  $https_port             = $safeshare::webapp::params::https_port,
  $ssl_cert_path          = $safeshare::webapp::params::ssl_cert_path,
  $ssl_cert_name          = $safeshare::webapp::params::ssl_cert_name,
  $ssl_key_name           = $safeshare::webapp::params::ssl_key_name,
  $cert_source_path       = $safeshare::webapp::params::cert_source_path,
  $include_robots_header  = $safeshare::webapp::params::include_robots_header,
) inherits safeshare::webapp::params
{

  if $access_service_url == undef {
    fail('access_service_url needs to be defined')
  }

  anchor { 'safeshare::webapp::begin': }
  -> class { 'safeshare::webapp::install':}
  -> class { 'safeshare::webapp::custom': }
  -> class { 'safeshare::webapp::config': }
  -> anchor { 'safeshare::webapp::end': }
}
