class safeshare::access_service::service inherits safeshare::access_service {
  service { 'access-service':
    ensure => $safeshare::access_service::service_ensure,
  }
}
