class safeshare::access_service::config inherits safeshare::access_service {
  File {
    ensure  => 'file',
    group   => 'access-service',
    owner   => 'access-service',
    mode    => '0600',
    notify  => Service['access-service'],
    require => Class['safeshare::access_service::install'],
  }

  file { "${safeshare::access_service::configuration_folder}/config.properties":
    content => template('safeshare/etc/access-service/config.properties.erb'),
  }

  file { "${safeshare::access_service::configuration_folder}/loggerConfig.xml":
    content => template('safeshare/etc/access-service/loggerConfig.xml.erb'),
    notify  => Service['access-service'],
  }

  file { "${safeshare::access_service::configuration_folder}/wrapper.conf":
    content => template("safeshare/etc/access-service/wrapper.${::osfamily}.conf.erb"),
  }

  file { "${safeshare::access_service::configuration_folder}/otpConfig.yml":
    content => template('safeshare/etc/access-service/otpConfig.yml.erb'),
    notify  => Service['access-service'],
  }

  file { "${safeshare::access_service::configuration_folder}/jwt_rsa_private_key.pem":
    source => $safeshare::access_service::jwt_rsa_private_key,
  }

  file { "${safeshare::access_service::configuration_folder}/jwt_rsa_public_key.pem":
    source => $safeshare::access_service::jwt_rsa_public_key,
  }
}
