class safeshare::access_service::params inherits safeshare::params {
  $version                          = 'latest'
  $custom_app_source_path           = undef
  $custom_notifications_source_path = undef
  $database_hostname                = undef
  $database_username                = undef
  $database_password                = undef
  $database_name                    = 'so'
  $database_port                    = 5432
  $access_service_url               = undef
  $content_service_url              = undef
  $webapp_service_url               = undef
  $import_allow_password            = false
  $mail_auth                        = undef
  $mail_password                    = undef
  $mail_username                    = undef
  $mail_hostname                    = undef
  $mail_starttls_enabled            = false
  $mail_ssl_enabled                 = undef
  $mail_port                        = undef
  $mail_from_address                = undef
  $java_xmx                         = '2G'
  $min_firefox_version              = undef
  $company_name                     = undef
  $master_encryption_key            = undef
  $bearer_signing_key               = undef
  $jwt_signing_key                  = undef
  $jwt_rsa_public_key               = undef
  $jwt_rsa_private_key              = undef
  $http_port                        = 8080
  $rabbitmq_user_name               = 'guest'
  $rabbitmq_password                = 'guest'
  $rabbitmq_host                    = 'localhost'
  $rabbitmq_port                    = 5672
  $rabbitmq_use_ssl                 = false
  $rabbitmq_keystore_password       = undef
  $rabbitmq_keystore_path           = undef
  $rabbitmq_truststore_password     = undef
  $rabbitmq_truststore_path         = undef
  $rabbitmq_vhost                   = undef
  $log_max_file_size                = undef
  $log_days_max_history             = undef
  $service_ensure                   = 'running'
  $enable_hsts                      = false
  $enable_hsts_subdomains           = false
  $custom_properties                = {}
  $configuration_folder             = '/etc/access-service'
  $otp_validity_minutes             = 20

  if ! member(['RedHat', 'Debian'], $::osfamily) {
    fail("unsupported osfamily \"${::osfamily}\"")
  }
}
