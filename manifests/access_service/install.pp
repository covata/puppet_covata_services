class safeshare::access_service::install inherits safeshare::access_service {
  include safeshare::repo

  package { 'access-service':
    ensure  => $safeshare::access_service::version,
    require => Class['safeshare::repo'] ,
    notify  => Service['access-service'],
  }
}
