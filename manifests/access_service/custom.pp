class safeshare::access_service::custom inherits safeshare::access_service {

  File {
    ensure  => 'directory',
    recurse => 'remote',
    group   => 'root',
    owner   => 'root',
    mode    => '0644',
    require => Class['safeshare::access_service::install'],
    notify  => Service['access-service'],
  }

  if $safeshare::access_service::custom_app_source_path {
    file { '/usr/share/access-service':
      source  => $safeshare::access_service::custom_app_source_path,
    }
  }

  if $safeshare::access_service::custom_notifications_source_path {
    file { '/etc/access-service/notifications':
      source  => $safeshare::access_service::custom_notifications_source_path,
    }
  }
}
