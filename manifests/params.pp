# Class: safeshare::params
#
# Safe Share common parameters.
#
# Parameters:
#  [*log_appenders*]
#    A list of log appenders Safe Share will use to write logs to
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class safeshare::params (
  $log_appenders = ['console', 'stdout'],
)
{
  $valid_appenders = ['console', 'stdout', 'logstash']
}
