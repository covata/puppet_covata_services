# Private Class. Do not use it directly

class safeshare::repo (
  $token = undef,
  $alternative_apt_repository = undef,
  $alternative_apt_key = undef,
  $alternative_apt_key_source = undef,
  $alternative_yum_repository = undef,
) {
  $version = safeshare_module_version()

  if $caller_module_name != $module_name {
    fail("Use of private class ${name} by ${caller_module_name}")
  }

  if $token != undef {
    if $alternative_apt_repository != undef or $alternative_apt_key != undef
      or $alternative_yum_repository != undef or $alternative_apt_key_source != undef {
      fail('token should only be defined when using default Covata repository. alternative_* options should be undefined')
    }
  }

  if $alternative_apt_repository == undef and $alternative_apt_key != undef {
      fail('alternative_apt_key should only be defined when defining alternative_apt_repository')
  }

  if $alternative_apt_key != undef and $alternative_apt_key_source == undef {
      fail('alternative_apt_key_source should only be defined when defining alternative_apt_key')
  }

  if $token {
    $apt_repo_url = "https://${token}@apt.fury.io/covata"
    $apt_release  = '/'
    $apt_repos    = ''
    $yum_repo_url = "https://${token}@yum.fury.io/covata/"
  } else {
    $apt_repo_url = $alternative_apt_repository
    $apt_release  = undef
    $apt_repos    = undef
    $yum_repo_url = $alternative_yum_repository
  }

  case $::osfamily {
    'debian': {


      if $apt_repo_url {
        include apt
        if $alternative_apt_key {
          apt::key { 'covata':
            key        => $alternative_apt_key,
            key_source => $alternative_apt_key_source,
            before     => Apt::Source['covata'],
          }
        }

        apt::source { 'covata':
          location       => $apt_repo_url,
          repos          => $apt_repos,
          include        => {
            src => false,
          },
          allow_unsigned => true,
          release        => $apt_release,
          comment        => 'Covata apt repository',
        }
        contain Apt

        apt::pin { "pinning safeshare version to ${version}":
          priority => '550',
          packages => ['access-service', 'access-service-migration', 'content-service', 'safe-share-for-web'],
          version  => "${version}.*"
        }
      } else {
        notice('Not installing apt repository')
      }
    }
    'RedHat': {
      if $yum_repo_url {
        yumrepo { 'covata':
          enabled     => 1,
          baseurl     => $yum_repo_url,
          gpgcheck    => 0,
          name        => 'covata',
          includepkgs => "access-service-${version}.* , \
                          access-service-migration-${version}.*, \
                          content-service-${version}.*, \
                          safe-share-for-web-${version}.*,\
                          java-service-wrapper-*",
          descr       => 'Covata yum repository',
        }
      } else {
        notice('Not installing yum repository')
      }
    }
    default: { fail("Unsupported family ${::osfamily}") }
  }
}
