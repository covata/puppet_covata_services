# Class: safeshare::content_service
#
# This class manages the SafeShare Content Service
#
# Parameters:
#
#  [*version*]
#    Optional. Default value is `latest`.
#    The SafeShare Content Service version to install.
#    Should be version 3.0 and above. Should include the full
#    version number (e.g. '3.0-965-1'). Can also be any possible
#    `Package` resource `ensure` value such as `latest` or `present`
#
#  [*access_service_url*]
#    URL of the Access Service, should have the same value as
#    `$safeshare::access_service::access_service_url`
#
#  [*webapp_service_url*]
#    URL of the Web App service
#
#  [*enable_multipart*]
#    Optional. Default value is `true`.
#    When enabled, the SafeShare clients will throttle download of content from
#    S3 by using multipart requests.
#    You might want to try to set it `false` if you encounter problems with your backing
#    S3 implementation. Setting this to 'false` will reduce the number of connections to
#    the S3 store but could cause flow control issues to your client if it fails to
#    process the downloaded data fast enough.
#    When this is enabled (default), the size of each request is controlled
#    via the `download_chunk_size` parameter (see below)
#
#  [*storage_type*]
#    Defines type of storage where the encrypted data is stored.
#    Can be `LOCAL` for local files on the content server's filesystem
#    or `S3COVATA` to use AWS S3 (or compatible implementation).
#    Default is `LOCAL`.
#    `LOCAL` requires `local_storage_folder` to be defined, `S3COVATA`
#    requires `s3_hostname`, `s3_access_key` and `s3_secret_key` to
#    be defined.
#
#  [*s3_hostname*]
#    Name of the S3 host to store the encrypted content on. Required iff
#    `storage_type` is `S3COVATA`.
#
#  [*s3_access_key*]
#    The AWS Access Key to access the S3 encrypted content on S3. Required iff
#    `storage_type` is `S3COVATA`.
#
#  [*s3_secret_key*]
#    The AWS Secret Key to access the S3 encrypted content on S3. Required iff
#    `storage_type` is `S3COVATA`.
#
#  [*data_bucket*]
#    This parameter has dual meaning. If `storage_type` is `S3COVATA` then
#    `data_bucket` holds the name of the S3 bucket in which the encrypted
#    content is stored.
#    If `storage_type` is `LOCAL` then `data_bucket` holds a prefix to
#    `local_storage_folder`
#
#  [*readonly_bucket*]
#    The name of a bucket that is used to store the encrypted preview files.
#
#  [*java_xmx*]
#    Optional. Java VM's `-Xmx` parameter (maximum size of memory allocation
#    pool in bytes).
#    Default is `2G`. May have to be increased if you are handling very large
#    files.
#
#  [*bearer_signing_key*]
#    Shared secret used to sign messages between the Access Service and the
#    Content Service during the initial handshake for generating a JSON
#    Web Token (JWT).
#    It must have the same value as
#    `$safeshare::access_service::bearer_signing_key`
#    It is a string of any random bytes.
#    Example command for generating it using `openssl`:
#
#    `openssl rand -base64 20`
#
#  [*download_chunk_size*]
#    Optional. Default is 10MB (`10*1024^2` bytes).
#    This is the maximum size of each chunk downloaded if `$enable_multipart`
#    is `true`. Specified as number of bytes.
#
#  [*upload_chunk_size*]
#    Optional. Default is 10MB (`10*1024^2` bytes).
#    This is the maximum size of each chunk uploaded.
#    Specified as number of bytes.
#
#  [*http_port*]
#    HTTP port on which Content Service will listen. Default `8081`.
#
#  [*rabbitmq_user_name*]
#    Optional. Default 'guest'.
#    Name of user used to access the RabbiqMQ service.
#
#  [*rabbitmq_password*]
#    Optional. Default 'guest'.
#    Password used to access the RabbitMQ service.
#
#  [*rabbitmq_host*]
#    Name or IP address of host of RabbitMQ service.
#
#  [*rabbitmq_port*]
#    (Optional) Port which the RabbitMQ service listens on. Default is `5672`.
#
#  [*rabbitmq_use_ssl*]
#    A boolean flag telling whether SSL should be used for communicating with
#    the RabbitMQ service (`true`) or not (`false`).
#    If this is `false` then `rabbitmq_keystore_password`,
#    `rabbitmq_keystore_path`, `rabbitmq_truststore_password` and
#    `rabbitmq_truststore_path` are ignored.
#
#  [*rabbitmq_keystore_path*]
#    Path to Java key-store containing key/certificate pair (PKCS12 format)
#    used to connect to the RabbitMQ service using SSL.
#    Used only if `rabbitmq_use_ssl` is set to `true`.
#
#  [*rabbitmq_keystore_password*]
#    Password for Java key-store containing key/certificate pair used
#    to connect to the RabbitMQ service.
#    Used only if `rabbitmq_use_ssl` is set to `true`.
#
#  [*rabbitmq_truststore_path*]
#    Path to Java key-store containing the X509 certificate of the trusted
#    RabbitMQ service.
#    Used only if `rabbitmq_use_ssl` is set to `true`.
#
#  [*rabbitmq_truststore_password*]
#    Password for Java key-store containing X509 certificate of RabbitMQ
#    service.
#    Used only if `rabbitmq_use_ssl` is set to `true`.
#
#  [*rabbitmq_vhost*]
#    (Optional) Virtual host to connect to for the RabbitMQ Service. Uses the default '/' otherwise'.
#
#  [*log_max_file_size*]
#    (Optional) Maximum log file size in bytes at which the file will be rotated.
#    Can be followed by optional suffixes 'KB', 'MB', 'GB'. Default is `30MB`.
#
#  [*log_days_max_history*]
#    Maximum number of days to keep previous log files.
#
#  [*local_storage_folder*]
#    Optional. Required if `storage_type` is `LOCAL`.
#    Path to the local folder under which encrypted data is stored.
#
#  [*manage_local_storage_folder*]
#    Optional. Default is 'true'. Only used if `storage_type` is `LOCAL`.
#    Set to 'false' if `local_storage_folder` should not be managed by this module.
#
#  [*service_ensure*]
#    Value to be passed to the service `ensure` attribute. Acceptable values
#    are 'running', 'stopped' or 'undef'.
#    Default is 'running'
#
#  [*rng_package_name*]
#    Name of RNG Tools package. Default depends on OS family.
#
#  [*rng_service_name*]
#    Name of RNG Tools service. Default is 'rng-tools'
#
#  [*rng_version*]
#    Version to install. Default depends on platform.
#
#  [*rng_service_hasstatus*]
#    Whether the service implements 'status'. Default depends on platform.
#
#  [*rng_service_status*]
#    How to test whether the service is running or not.
#    Default depends on platform.
#
#  [*custom_properties*]
#    Map of custom server properties to be added to the server config.conf file
#
#  [*log_appenders*]
#    (Optional) A list of log appenders Content Service will use to write logs to
#    Default: ['console', 'stdout']
#    Valid options are listed in Class: safeshare::params
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
class safeshare::content_service (
  $version                      = $safeshare::content_service::params::version,
  $access_service_url           = $safeshare::content_service::params::access_service_url,
  $webapp_service_url           = $safeshare::content_service::params::webapp_service_url,
  $enable_multipart             = $safeshare::content_service::params::enable_multipart,
  $storage_type                 = $safeshare::content_service::params::storage_type,
  $s3_hostname                  = $safeshare::content_service::params::s3_hostname,
  $s3_access_key                = $safeshare::content_service::params::s3_access_key,
  $s3_secret_key                = $safeshare::content_service::params::s3_secret_key,
  $data_bucket                  = $safeshare::content_service::params::data_bucket,
  $readonly_bucket              = $safeshare::content_service::params::readonly_bucket,
  $java_xmx                     = $safeshare::content_service::params::java_xmx,
  $bearer_signing_key           = $safeshare::content_service::params::bearer_signing_key,
  $download_chunk_size          = $safeshare::content_service::params::download_chunk_size,
  $upload_chunk_size            = $safeshare::content_service::params::upload_chunk_size,
  $http_port                    = $safeshare::content_service::params::http_port,
  $rabbitmq_user_name           = $safeshare::content_service::params::rabbitmq_user_name,
  $rabbitmq_password            = $safeshare::content_service::params::rabbitmq_password,
  $rabbitmq_host                = $safeshare::content_service::params::rabbitmq_host,
  $rabbitmq_port                = $safeshare::content_service::params::rabbitmq_port,
  $rabbitmq_use_ssl             = $safeshare::content_service::params::rabbitmq_use_ssl,
  $rabbitmq_keystore_password   = $safeshare::content_service::params::rabbitmq_keystore_password,
  $rabbitmq_keystore_path       = $safeshare::content_service::params::rabbitmq_keystore_path,
  $rabbitmq_truststore_password = $safeshare::content_service::params::rabbitmq_truststore_password,
  $rabbitmq_truststore_path     = $safeshare::content_service::params::rabbitmq_truststore_path,
  $rabbitmq_vhost               = $safeshare::content_service::params::rabbitmq_vhost,
  $log_max_file_size            = $safeshare::content_service::params::log_max_file_size,
  $log_days_max_history         = $safeshare::content_service::params::log_days_max_history,
  $local_storage_folder         = $safeshare::content_service::params::local_storage_folder,
  $manage_local_storage_folder  = $safeshare::content_service::params::manage_local_storage_folder,
  $service_ensure               = $safeshare::content_service::params::service_ensure,
  $rng_package_name             = $safeshare::content_service::params::rng_package_name,
  $rng_service_name             = $safeshare::content_service::params::rng_service_name,
  $rng_version                  = $safeshare::content_service::params::rng_version,
  $rng_service_hasstatus        = $safeshare::content_service::params::rng_service_hasstatus,
  $rng_service_status           = $safeshare::content_service::params::rng_service_status,
  $custom_properties            = $safeshare::content_service::params::custom_properties,
  $log_appenders                = $safeshare::content_service::params::log_appenders,
  $azurebs_connectionstring     = $safeshare::content_service::params::azurebs_connectionstring,
) inherits safeshare::content_service::params {
  include safeshare::params

  if $access_service_url == undef {
    fail('access_service_url needs to be defined')
  }
  if $webapp_service_url == undef {
    fail('webapp_service_url needs to be defined')
  }

  validate_bool($enable_multipart)

  if ! member(['LOCAL', 'S3COVATA', 'AZUREBS'], $storage_type) {
    fail("unsupported storage_type \"${storage_type}\"")
  }

  $local_storage_enabled = ($storage_type == 'LOCAL')
  $s3_storage_enabled = ($storage_type == 'S3COVATA')
  $azure_storage_enabled = ($storage_type == 'AZUREBS')
  $manage_storage_folder = ($local_storage_enabled and $manage_local_storage_folder)

  if $data_bucket == undef {
    fail('data_bucket needs to be defined')
  }

  if $readonly_bucket == undef {
    fail('readonly_bucket needs to be defined')
  }

  if $bearer_signing_key == undef {
    fail('bearer_signing_key needs to be defined')
  }

  if $local_storage_enabled {
    if $local_storage_folder == undef {
      fail('local_storage_folder needs to be defined for local storage type')
    }
  }
  if $s3_storage_enabled {
    if $s3_hostname == undef {
      fail('s3_hostname needs to be defined for S3 storage type')
    }

    if $s3_access_key == undef {
      fail('s3_access_key needs to be defined for S3 storage type')
    }

    if $s3_secret_key == undef {
      fail('s3_secret_key needs to be defined for S3 storage type')
    }
  }
  if $azure_storage_enabled {
    if $azurebs_connectionstring == undef {
      fail('azurebs_connectionstring needs to be defined for Azure storage type')
    }
  }

  validate_numeric($http_port)

  validate_bool($rabbitmq_use_ssl)

  if $rabbitmq_use_ssl {
    if $rabbitmq_keystore_path == undef {
      fail('rabbitmq_use_ssl is enabled so rabbitmq_keystore_path needs to be defined')
    }
    if $rabbitmq_keystore_password == undef {
      fail('rabbitmq_use_ssl is enabled so rabbitmq_keystore_password needs to be defined')
    }

    if $rabbitmq_truststore_path == undef {
      fail('rabbitmq_use_ssl is enabled so rabbitmq_truststore_path needs to be defined')
    }
    if $rabbitmq_truststore_password == undef {
      fail('rabbitmq_use_ssl is enabled so rabbitmq_truststore_password needs to be defined')
    }
  }

  validate_bool($manage_local_storage_folder)

  validate_re($log_max_file_size, '^[0-9]*([KMG]B)?$',
    'log_max_file_size must be numeric with optional "KB" or "MB" or "GB" suffix')

  validate_numeric($log_days_max_history)

  validate_array($log_appenders)
  if ! member($safeshare::params::valid_appenders, $log_appenders) {
    $invalid_appenders = join($log_appenders, ',')
    $allowed_appenders = join($safeshare::params::valid_appenders, ',')
    fail("log_appenders contains invalid values \"${invalid_appenders}\". Valid values are \"${allowed_appenders}\".")
  }

  anchor{ 'safeshare::content_service::begin': }
  -> class { 'safeshare::content_service::install': }
  -> class { 'safeshare::content_service::config': }
  -> class { 'safeshare::content_service::service': }
  -> anchor{ 'safeshare::content_service::end': }
}
