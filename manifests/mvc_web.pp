# Unsupported for 3.X
class safeshare::mvc_web {

  fail('unsupported mvc_web for this version')

  include safeshare::mvc_web::package
  include safeshare::mvc_web::config
  include safeshare::mvc_web::service

  anchor {'safeshare::mvc_web_start': }
  -> Class[mvc_web::package]
  -> Class[mvc_web::config]
  -> Class[mvc_web::service]
  -> anchor {'safeshare::mvc_web_end': }
}
