class safeshare::access_service_migration::install inherits safeshare::access_service_migration
{
  include safeshare::repo

  package {'access-service-migration':
    ensure  => $safeshare::access_service_migration::version,
    require => Class['safeshare::repo'],
    notify  => Class['safeshare::access_service_migration::migration'],
  }
}
