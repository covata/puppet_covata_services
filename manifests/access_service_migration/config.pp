class safeshare::access_service_migration::config inherits safeshare::access_service_migration
{
  File {
    ensure  => 'file',
    group   => 'root',
    owner   => 'root',
    mode    => '0600',
    notify  => Exec['access-service-migration'],
    require => Package['access-service-migration'],
  }

  file {'/etc/access-service-migration/config.yml':
    content => template('safeshare/etc/access-service-migration/config.yml.erb'),
  }

  file {'/etc/access-service-migration/logback.xml':
    content => template('safeshare/etc/access-service-migration/logback.xml.erb'),
  }

}
