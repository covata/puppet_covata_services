class safeshare::access_service_migration::migration inherits safeshare::access_service_migration
{
  exec {'access-service-migration':
    command     => '/usr/bin/java -jar -Dlogging.config=/etc/access-service-migration/logback.xml /usr/share/access-service-migration/access-service-migration.jar --spring.config.location=/etc/access-service-migration/config.yml',
    environment => ['SO_MASTER_KEY=password'],
    refreshonly => true,
  }
}
