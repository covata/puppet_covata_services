class safeshare::access_service_migration::params inherits safeshare::params
{
  $version               = 'latest'
  $access_service_url    = undef
  $database_password     = undef
  $webapp_service_url    = undef
  $database_hostname     = 'localhost'
  $database_name         = 'so'
  $database_port         = 5432
  $log_max_file_size     = '30MB'
  $log_days_max_history  = 30

  if ! member(['RedHat', 'Debian'], $::osfamily) {
    fail("unsupported osfamily \"${::osfamily}\"")
  }
}
