class safeshare::content_service::install inherits safeshare::content_service {

  include safeshare::repo

  package { 'content-service':
    ensure  => $safeshare::content_service::version,
    notify  => Service['content-service'],
    require => Class['safeshare::repo'],
  }

  if $safeshare::content_service::rng_package_name {
    package { $safeshare::content_service::rng_package_name:
      ensure => $safeshare::content_service::rng_version,
    }
  }

  if $safeshare::content_service::rng_service_name {
    if ! $safeshare::content_service::rng_package_name {
      fail("\$rng_service_name requires \$rng_package_name to be defined")
    }
    else {
      service { $safeshare::content_service::rng_service_name:
        ensure     => 'running',
        enable     => true,
        hasrestart => true,
        hasstatus  => $safeshare::content_service::rng_service_hasstatus,
        status     => $safeshare::content_service::rng_service_status,
        require    => Package[$safeshare::content_service::rng_package_name],
      }
    }
  }
}
