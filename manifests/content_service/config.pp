class safeshare::content_service::config inherits safeshare::content_service {
  File {
    group   => 'content-service',
    owner   => 'content-service',
    mode    => '0600',
  }

  file {'/etc/content-service/config.conf':
    ensure  => 'file',
    content => template('safeshare/etc/content-service/config.conf.erb'),
    notify  => Service['content-service'],
  }

  file {'/etc/content-service/loggerConfig.xml':
    ensure  => 'file',
    content => template('safeshare/etc/content-service/loggerConfig.xml.erb'),
    notify  => Service['content-service'],
  }

  file {'/etc/content-service/wrapper.conf':
    ensure  => 'file',
    content => template("safeshare/etc/content-service/wrapper.${::osfamily}.conf.erb"),
    notify  => Service['content-service'],
  }

  if $safeshare::content_service::manage_storage_folder{
    file { $safeshare::content_service::local_storage_folder:
      ensure => 'directory',
      mode   => '0700',
    }
  }
}
