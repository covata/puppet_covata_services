class safeshare::content_service::params inherits safeshare::params {
  $version                      = 'latest'
  $access_service_url           = undef
  $webapp_service_url           = undef
  $enable_multipart             = true
  $storage_type                 = 'LOCAL'
  $s3_hostname                  = undef
  $s3_access_key                = undef
  $s3_secret_key                = undef
  $data_bucket                  = undef
  $readonly_bucket              = undef
  $java_xmx                     = '2G'
  $bearer_signing_key           = undef
  $download_chunk_size          = undef
  $upload_chunk_size            = undef
  $http_port                    = 8081
  $rabbitmq_user_name           = 'guest'
  $rabbitmq_password            = 'guest'
  $rabbitmq_host                = 'localhost'
  $rabbitmq_port                = 5672
  $rabbitmq_use_ssl             = false
  $rabbitmq_keystore_password   = undef
  $rabbitmq_keystore_path       = undef
  $rabbitmq_truststore_password = undef
  $rabbitmq_truststore_path     = undef
  $rabbitmq_vhost               = undef
  $log_max_file_size            = '30MB'
  $log_days_max_history         = 30
  $local_storage_folder         = '/var/lib/content-service-storage'
  $manage_local_storage_folder  = true
  $service_ensure               = 'running'
  $custom_properties            = {}
  $rng_package_name             = 'rng-tools'
  $rng_version                  = 'installed'
  $azurebs_connectionstring     = undef

  case $::osfamily {
    'Debian': {
      $rng_service_name = 'rng-tools'
      $rng_service_hasstatus = false
      $rng_service_status = '/usr/bin/pgrep rngd'
    }
    'RedHat': {
      $rng_service_name = 'rngd'
      $rng_service_hasstatus = true
      $rng_service_status = undef
    }
    default: {
      fail("unsupported osfamily \"${::osfamily}\"")
    }
  }
}
