class safeshare::content_service::service inherits safeshare::content_service {
  service { 'content-service':
    ensure => $safeshare::content_service::service_ensure,
  }
}
